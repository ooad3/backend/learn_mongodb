const mongoose = require('mongoose')
const Room = require('./model/Room')
const Building = require('./model/Building')
mongoose.connect('mongodb://localhost:27017/example')

async function main () {
  // Find and Update
  // const room = await Room.findById('621bd5cf23c890ecb77a7fe7')
  // room.capacity = 20
  // room.save()
  // console.log(room)
  // FindOne can find data by field 1 field
  // const room = await Room.findOne({ _id: '621bd5cf23c890ecb77a7fe7' })
  // console.log(room)
  // Find can find data by fields
  const rooms = await Room.find({ capacity: { $gte: 100 } }).populate('building')
  console.log(rooms)
  // DeleteOne
  // const rooms = await Room.findOneAndDelete({ capacity: { $gte: 100 } })
  // console.log(rooms)
}

main().then(() => {
  console.log('Finish')
})
