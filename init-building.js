const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./model/Room')
const Building = require('./model/Building')

async function clearDb () {
  await Room.deleteMany({})
  await Building.deleteMany({})
}

async function main () {
  await clearDb()
  const informaticsBuilding = new Building({ name: 'Informatics', floor: 11 })
  const newInformaticBuilding = new Building({ name: 'New Infornatics', floor: 20 })
  const room3c01 = new Room({ name: '3c01', capacity: 200, floor: 3, building: informaticsBuilding })
  informaticsBuilding.room.push(room3c01)
  const room4c01 = new Room({ name: '4c01', capacity: 150, floor: 4, building: informaticsBuilding })
  informaticsBuilding.room.push(room4c01)
  const room5c01 = new Room({ name: '5c01', capacity: 200, floor: 5, building: informaticsBuilding })
  informaticsBuilding.room.push(room5c01)
  await informaticsBuilding.save()
  await newInformaticBuilding.save()
  await room3c01.save()
  await room4c01.save()
  await room5c01.save()
}

main().then(function () {
  console.log('Finish')
})
